package com.unblau.javajwt.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JWTHeader implements Serializable
{
	private static final long serialVersionUID = 1L;

	/**
	 * @see <a href="http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#typHdrDef">Type Header Parameter</a>
	 */
	private String typ;
	
	private String alg;
	
	public JWTHeader() {}
	
	public String getTyp() {return typ;}
	public void setTyp(String typ) {this.typ = typ;}
	
	public String getAlg() {return alg;}
	public void setAlg(String alg) {this.alg = alg;}
}